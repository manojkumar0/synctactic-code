from pyspark import SparkContext

# method that counts the occurrence of words
def wordCount():
    sc=SparkContext(appName="local")
    rdd=sc.parallelize(["a" ,"b", "c","b","a","d","c"])
    rdd1=rdd.flatMap(lambda l:l.split(" ")).\
        map(lambda l: (l,1)).\
        reduceByKey(lambda l,m:l+m);
    print(rdd1.collect())

# starting point for the execution of program
if __name__ == "__main__":
    wordCount()
