## What is PySpark?
PySpark is a python API for spark released by Apache Spark community to support python with Spark.
## Docker-PySpark (Jupiter Notebook)

This Docker image `jupyter/pyspark-notebook`helps you to run PySpark (in Docker)

###  How to install on ubuntu
---
 #### 1. Get the latest update of packages
  ```
  sudo apt-get update
 ```
 #### 2. Pull the docker image
  ```
docker pull jupyter/pyspark-notebook
 ```
 #### 2. Start the container 
Run the following command to start the container and to get into bash prompt
```
sudo docker run -it jupyter/pyspark-notebook bash
 ```
 #### 3. Start PySpark
Create a new python file ```wordcount.py``` and write pyspark code.
 #### 3. Execute the pyspark code 
 spark-submit command is available in 
 /usr/local/spark/bin/spark-submit 
 ```
 /usr/local/spark/bin/spark-submit wordcount.py
 ```
