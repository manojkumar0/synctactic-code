### To copy files from local machine to docker container

```
sudo docker cp [local file] [container-id]:[location in container]
```
  #### Example
  sudo docker cp helloworld.py  e5583194e42e:/home/jovyan/helloworld.py