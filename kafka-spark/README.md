
## Kafka-Spark connect using python

### What is Kafka?

 Kafka is **a distributed publish-subscribe messaging system that maintains feeds of messages in partitioned and replicated topics**. In the simplest way there are three players in the Kafka ecosystem: producers, topics (run by brokers) and consumers.
 
![](https://miro.medium.com/max/622/1*48ck-bvatKzEpVapVa4Mag.png)
### Topics
Every message that is feed into the system must be part of some topic. The topic is nothing but a stream of records. The messages are stored in key-value format. Each message is assigned a sequence, called _Offset_. The output of one message could be an input of the other for further processing.

### Producers
Producers are the apps responsible to publish data into Kafka system. They publish data on the topic of their choice.

### Consumers
The messages published into topics are then utilized by _Consumers_ apps. A consumer gets subscribed to the topic of its choice and consumes data.

### Broker
Every instance of Kafka that is responsible for message exchange is called a Broker. Kafka can be used as a stand-alone machine or a part of a cluster.
##
### Installation
 #### 1. Download and extract Apache Kafka 
 
http://mirrors.estointernet.in/apache/kafka/2.2.0/kafka_2.11-2.2.0.tgz
 
 #### 2. Start Zookeeper and broker server in Apache Kafka by entering into kafka extracted folder
 ```
bin/zookeeper-server-start.sh config/zookeeper.properties 
bin/kafka-server-start.sh config/server.properties
 ```

 #### 3. Create a topic 
 ##### (here spark-topic is topic name and partitions size is 1 and replication is 1)
```
bin/kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic spark-topic
 ```
##
### Code:
####  KafkaProducer: 
KafkaProducer is a high-level, asynchronous message producer.

Bootstrap Servers are a list of host/port pairs to use for establishing the initial connection to the Kafka cluster.
Here spark-topic1 is the topic name that producer is producing to. When producer is sending the data to topic both key and value should be in byte format. We used flush() because Kafka clients send messages in batches, not immediately to reduce load on the brokers.

````
from kafka import KafkaProducer
producer = KafkaProducer(bootstrap_servers='localhost:9092')  
producer.send('spark-topic1', key=b"list_names",  
              value=bytes(str(name_list), encoding='utf8'))  
producer.flush()
````

####  KafkaConsumer:
![](https://miro.medium.com/max/642/1*Pp5vDC3T6OVWMHeWLdiIqA.png)

Here spark-topic2 is the topic name that consumer is consuming from
````
from kafka import KafkaConsumer
import ast

consumer = KafkaConsumer('spark-topic2',  
                         bootstrap_servers='localhost:9092')  
for message in consumer:  
	key=message.key.decode("utf-8")
	print("key is: "+key )
	value=ast.literal_eval(message.value.decode("utf-8"))
	print("value is: "+value)
````
##
#### Spark Streaming:
Spark Streaming provides a way of processing "unbounded" data - commonly referred to as "streaming" data. It does this by breaking it up into microbatches, and supporting windowing capabilities for processing across multiple batches.

![Kafka and Spark streaming pipeline](https://tlfvincent.github.io/img/kafka_spark_pipeline.png)

Inorder to run spark streaming python file  we need to have 
spark streaming kafka jar file

jar download link:
https://mvnrepository.com/artifact/org.apache.spark/spark-streaming-kafka-0-8-assembly_2.10/2.0.0-preview
````
spark-submit --jars spark-streaming-kafka-0-8-assembly_2.11-2.1.0.jar kafka_spark_streaming.py
````

The Spark context is the primary object under which everything else is called. Here `setLogLevel` call is set to OFF. We pass the Spark context object along with the batch duration which here is set to 5 seconds to the `StreamingContext`. We are using **_Direct approach_** to create Directstream. 

handler => is the python method.

````
from pyspark.streaming import StreamingContext  
from pyspark.streaming.kafka import KafkaUtils  
from pyspark import SparkContext, SQLContext

sc = SparkContext(appName="spark_stream")  
sc.setLogLevel("OFF")  
ssc = StreamingContext(sc, 5)  
topic = "spark-topic1"  
brokers = "localhost:9092"  
kvs = KafkaUtils.createDirectStream(ssc, [topic],  
                                    {"metadata.broker.list": brokers})  
kvs.foreachRDD(lambda rdd: rdd.foreachPartition(handler))  
ssc.start()  
ssc.awaitTermination()
````

#### python handler() method
message is of type tuple (message[0] gets us key and message[1] gets value from the topic
````
def handler(messages):  
    producer = KafkaProducer(bootstrap_servers='localhost:9092')  
  
    for message in messages:  
	list_values = ast.literal_eval(message[1])  
	
	[
	 # Processing of data and 
	 # producing data to another topic
	]
	  
	 producer.send('spark-topic2',  
                      key=bytes(str("name_list"),encoding='utf8'),  
                      value=message[1].encode('utf-8'))  
         producer.flush()
````
