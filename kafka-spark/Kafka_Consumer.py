from kafka import KafkaConsumer
import ast
import pandas as pd
import boto3
data_list = []
header_list = []


def final_dataframe(data_list1, header_list1):
    pandas_df = pd.DataFrame(data_list1, columns=header_list1)
    # converting to csv file
    pandas_df.to_csv("/home/syn005/Downloads/processed_kafka_records.csv", index=False)
    try:
        s3 = boto3.client(
            "s3",
            aws_access_key_id="",
            aws_secret_access_key="")

        # for uploading file to s3 ,we are using client which has upload_file component
        s3.upload_file("/home/syn005/Downloads/processed_kafka_records.csv",
                      "sampleds",
                      "processed_kafka_records.csv")
        print("upload successful")
    except Exception as e:
        print(e)

# starting point for the execution of program
if __name__ == "__main__":
    consumer = KafkaConsumer('spark-topic2',
                             bootstrap_servers='localhost:9092')
    for message in consumer:
        if message.key.decode("utf-8") == 'column_names':
            string_list = message.value.decode("utf-8")
            header_list = (ast.literal_eval(string_list))
        elif message.key.decode("utf-8") == 'after_last_row':
            final_dataframe(data_list, header_list)
        else:
            string_list = message.value.decode("utf-8")
            actual_list = ast.literal_eval(string_list)
            data_list.append(actual_list)
