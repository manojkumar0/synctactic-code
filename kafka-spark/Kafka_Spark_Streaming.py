from pyspark.sql.types import StructField, StringType, StructType

from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils
from pyspark import SparkContext, SQLContext
from kafka import KafkaProducer
import ast
import pandas as pd

# schema of dataframe
pandas_header = None

spark_header = None


def spark_header_creation(column, column_header):
    global spark_header

    if column == 'column_names':
        struct_fields = [StructField(col_name, StringType(), True) for col_name
                         in
                         column_header]
        mySchema = StructType(struct_fields)
        spark_header = mySchema
        return spark_header

    return spark_header


# creation of dataframe with header
def pandas_header_creation(column, column_header):
    # if key of message from topic matches
    if column == 'column_names':
        global pandas_header
        pandas_header = column_header
    return pandas_header


def handler(messages):
    producer = KafkaProducer(bootstrap_servers='localhost:9092')
    print("--inside handler")

    # message is of type tuple(message[0] gets us key and message[1] gets value
    for message in messages:
        # converting str to list
        list_values = ast.literal_eval(message[1])

        # using spark dataframe
        # sp_header = spark_header_creation(str(message[0]), list_values)
        # sc = SparkContext.getOrCreate()
        # sqlContext = SQLContext(sc)
        # if str(message[0]) != "column_names" and str(
        #         message[0]) != "after_last_row":
        #     spark_df = sqlContext.createDataFrame([list_values],
        #                                           schema=sp_header)
        #     spark_df = spark_df.filter(
        #         spark_df['temperature'] != 'nan').filter(
        #         spark_df['temperature'] > 23.6)
        #     if not spark_df.rdd.isEmpty():
        #         producer.send('spark-topic2',
        #                       key=bytes(str("1"), encoding='utf8'),
        #                       value=message[1].encode('utf-8'))
        #         producer.flush()
        #
        # elif str(message[0]) == "after_last_row":
        #     producer.send('spark-topic2',
        #                   key=bytes(str("after_last_row"), encoding='utf8'),
        #                   value=message[1].encode('utf-8'))
        #     producer.flush()

        # pandas header creation
        pd_header = pandas_header_creation(str(message[0]), list_values)
        sc = SparkContext.getOrCreate()
        if str(message[0]) != "column_names" and \
                str(message[0]) != "after_last_row":
            pandas_df = pd.DataFrame([list_values], columns=pd_header)
            # applying filter on pandas dataframe
            filtered_pandas_df = pandas_df[pandas_df['Name Prefix'] == 'Ms.']

            # if pandas dataframe is not empty
            if not filtered_pandas_df.empty:
                print(filtered_pandas_df)
                producer.send('spark-topic2',
                              key=bytes(str("dataframe"),encoding='utf8'),
                              value=message[1].encode('utf-8'))
                producer.flush()

        elif str(message[0]) == "after_last_row":
            producer.send('spark-topic2',
                          key=bytes(str("after_last_row"),encoding='utf8'),
                          value=message[1].encode('utf-8'))
            producer.flush()


# main method
def main():
    sc = SparkContext(appName="spark_stream")
    sc.setLogLevel("OFF")
    # spark streaming is listening every 5 seconds as we mentioned 5
    ssc = StreamingContext(sc, 5)
    topic = "spark-topic1"
    # here 9092 is zookeeper port number
    brokers = "localhost:9092"
    kvs = KafkaUtils.createDirectStream(ssc, [topic],
                                        {"metadata.broker.list": brokers})
    kvs.foreachRDD(lambda rdd: rdd.foreachPartition(handler))
    ssc.start()
    ssc.awaitTermination()


if __name__ == "__main__":
    main()
