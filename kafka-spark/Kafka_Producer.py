import boto3
from io import StringIO
from kafka import KafkaProducer
import pandas as pd
import json


# s3 login connection
def s3_login():
    try:
        s3 = boto3.client(
            "s3",
            aws_access_key_id="",
            aws_secret_access_key="")
    except Exception as e:
        print(e)
    return s3


# fetches the data from provided s3 bucket file
def s3_data_fetch(s3):
    r = s3.select_object_content(
        Bucket="sampleds",
        Key="records.csv",
        ExpressionType='SQL',
        Expression=
        "select * from s3Object s limit 10000",
        InputSerialization={
            'CSV': {
                "FileHeaderInfo": "None",
                "FieldDelimiter": ","
            }
        },
        OutputSerialization={
            'CSV': {
                "FieldDelimiter": ","
            }}, )

    record_data = []
    for event in r['Payload']:
        if 'Records' in event:
            record_data.append(event['Records']['Payload'])

    decode_records = ''.join(r.decode("utf-8") for r in record_data)
    return decode_records


# processes the data which has retrieved from s3 bucket file
def data_processing(record_data):
    pandas_df = pd.read_csv(StringIO(record_data), sep='[,]', header=0)
    print(pandas_df)

    Row_list1 = []

    # Iterate over each row
    for i in range((pandas_df.shape[0])):
        # create a list to store the data of the current row
        cur_row = []

        # iterate over all the columns
        for j in range(pandas_df.shape[1]):
            # append the data of each
            # column to the list
            cur_row.append(str(pandas_df.iat[i, j]))

            # append the current row to the list
        Row_list1.append(cur_row)
        # Print the list

    column_list_unicode = list(pandas_df.columns.values.tolist())

    # converting unicode values to string
    header_list = json.dumps(column_list_unicode)
    producer = KafkaProducer(bootstrap_servers='localhost:9092')
    producer.send('spark-topic1', key=b"column_names",
                  value=bytes(str(header_list), encoding='utf8'))
    producer.send('spark-topic2',
                  key=bytes(str("column_names"), encoding='utf-8'),
                  value=bytes(str(header_list), encoding='utf8'))
    producer.flush()

    for i in range(0, len(Row_list1)):
        value_bytes = bytes(str(Row_list1[i]), encoding='utf8')
        producer.send('spark-topic1', key=b'row_values',
                      value=value_bytes)
        producer.flush()
    # topic name is spark-topic1
    producer.send('spark-topic1', key=b'after_last_row',
                  value=bytes(str(header_list), encoding='utf8'))
    producer.flush()
    try:
        print()
    except Exception as e:
        print(e)


# starting point for the execution of program
if __name__ == "__main__":
    s3 = s3_login()
    record_data = s3_data_fetch(s3)
    data_processing(record_data)
