import boto3
from io import StringIO
import pandas as pd
from pyspark import SparkContext
from pyspark.sql import SQLContext
from pyspark.sql.functions import col

#profile_name in credentials file
session = boto3.session.Session(profile_name="manu")

access_key = session.get_credentials().access_key
secret_key = session.get_credentials().secret_key
try:
    s3 = boto3.resource(
        "s3", aws_access_key_id=access_key, aws_secret_access_key=secret_key)
except Exception as e:
    print(e)

s3_binary_obj = s3.Object(bucket_name="sampleds", key="records.csv")
s3_obj = StringIO(s3_binary_obj.get()['Body'].read().decode("utf-8"))

#it considers first row in the file as header(column names) as header=0
pandas_df = pd.read_csv(s3_obj, sep='[,]', header=0)

sc = SparkContext("local", "s3_bucket_data_processing_app")
sqlContext = SQLContext(sc)

spark_df = sqlContext.createDataFrame(pandas_df)
spark_df1 = spark_df.filter((spark_df['`Age in Yrs.`'] > 50) &
                            (spark_df['Year of Joining'] > 2012))
pandas_df1 = spark_df1.toPandas()

pandas_df1.to_csv(
    "/home/jovyan/work/python_files/processed_records.csv", index=False)

try:
    #for uploading file to s3 ,we are using client as resource do not have upload_file component
    s3.meta.client.upload_file(
        "/home/jovyan/work/python_files/processed_records.csv", "sampleds",
        "processed_records.csv")
    print("upload successful")
except Exception as e:
    print(e)
