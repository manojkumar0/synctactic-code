import argparse
import boto3
from io import StringIO
import pandas as pd
from pyspark import SparkContext
from pyspark.sql import SQLContext

# s3 login connection
def s3_login():
    s3=''
    try:
        s3 = boto3.resource(
            "s3",
            aws_access_key_id=args_dict.get('access_key'),
            aws_secret_access_key=args_dict.get('secret_key'))
    except Exception as e:
        print(e)
    return s3

def s3_data_fetch_process(s3):
    s3_obj = s3.Object(
        bucket_name=args_dict.get('source_bucket_name'),
        key=args_dict.get('source_file_name'))
    data = StringIO(s3_obj.get()['Body'].read().decode('utf-8'))

    df = pd.read_csv(data, sep='[,;]', header=0)

    spark_df = sqlContext.createDataFrame(df)
    spark_df.registerTempTable("tempTable")
    spark_df1 = sqlContext.sql("select * from tempTable where " + args_dict.get(
        'filter_key1') + " and " + args_dict.get('filter_key2'))

    spark_df1.show()

    pandas_df1 = spark_df1.toPandas()

    pandas_df1.to_csv("/home/jovyan/work/delete/text.txt", index=False)

    try:
        # for uploading file to s3 ,we are using client as resource do not have 
        # upload_file component
        s3.meta.client.upload_file("/home/jovyan/work/delete/text.txt",
                                   args_dict.get('destination_bucket_name'),
                                   args_dict.get('destination_file_name'))
        print("upload successful")
    except Exception as e:
        print(e)

#starting point for the execution of program
if __name__== "__main__":
    sc = SparkContext("local", "python_aws_s3")
    sqlContext = SQLContext(sc)

    parser = argparse.ArgumentParser(description="aws_python_args")

    parser.add_argument('access_key', help="access key")
    parser.add_argument('secret_key', help="secret key")
    parser.add_argument('source_bucket_name', help="bucket name")
    parser.add_argument('source_file_name', help="file name")
    parser.add_argument('destination_bucket_name', help='destination bucket name')
    parser.add_argument('destination_file_name', help='destination file name')
    parser.add_argument('filter_key1', help="filter key 1")
    parser.add_argument('filter_key2', help="filter key 2")

    args = parser.parse_args()
    print(vars(args))
    args_dict = vars(args)
    s3=s3_login()
    s3_data_fetch_process(s3)
