#!/bin/bash
#Created on September 24, 2019

pythonFile="awspython.py"

#----------------------checking file exists or not-------------------------

if [ ! -f $pythonFile ];then
 echo "The file $pythonFile is not available at $PWD location"
 exit 1
fi

#----------------------validating the length of arguments------------------

length_validation() {
argument=$1
if [ "$argument" = "" ];then
 echo "entered argument is null please enter valid argument"
 exit 1
fi
}

#-----------------------user arguments-------------------------------------

read -p "access_key :" access_key
length_validation $access_key

read -p "secret_key :" secret_key
length_validation $secret_key

read -p "source_bucket_name :" source_bucket_name
length_validation $source_bucket_name

read -p "source_file_name :" source_file_name
length_validation $source_file_name

read -p "destination_bucket_name :" destination_bucket_name
length_validation $destination_bucket_name

read -p "destination_file_name :" destination_file_name
length_validation $destination_file_name

read -p "filter_key1 :" filter_key1
length_validation $filter_key1

read -p "filter_key2 :" filter_key2
length_validation $filter_key2

python $pythonFile $access_key $secret_key $source_bucket_name $source_file_name $destination_bucket_name $destination_file_name $filter_key1 $filter_key2
