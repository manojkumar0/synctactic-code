import argparse
import boto3
from io import StringIO
import pandas as pd
from pyspark import SparkContext
from pyspark.sql import SQLContext

#s3 login connection
def s3_login():
    s3=''
    try:
        s3 = boto3.client(
            "s3",
            aws_access_key_id=args_dict.get('access_key'),
            aws_secret_access_key=args_dict.get('secret_key'))
    except Exception as e:
        print(e)
    return s3

#fetches the data from provided s3 bucket file
def s3_data_fetch(s3):
    r = s3.select_object_content(
        Bucket=args_dict.get('source_bucket_name'),
        Key=args_dict.get('source_file_name'),
        ExpressionType='SQL',
        Expression=
        "select * from s3Object s where s.name in ['manoj','kumar','varun'] and"
        " city='ysr'",
        InputSerialization={
           'CSV': {
                "FileHeaderInfo": "Use",
                "FieldDelimiter": ";"
         }
        },
        OutputSerialization={
            'CSV': {
            "FieldDelimiter": ";"
     }},)

    record_data = []
    for event in r['Payload']:
        if 'Records' in event:
            record_data.append(event['Records']['Payload'])

    decode_records=''.join(r.decode("utf-8") for r in record_data)
    return decode_records

#processes the data which has retrieved from s3 bucket file
def data_processing(record_data):

    df = pd.read_csv(StringIO(record_data), sep='[;]', names=['name', 'age', 'city'])

    spark_df = sqlContext.createDataFrame(df)
    spark_df.registerTempTable("tempTable")
    spark_df1 = sqlContext.sql("select * from tempTable where " + args_dict.get(
       'filter_key1') + " and " + args_dict.get('filter_key2'))

    pandas_df1 = spark_df1.toPandas()

    pandas_df1.to_csv("/home/jovyan/work/awspython/text.txt", index=False)

    try:
        # for uploading file to s3 ,we are using client which has upload_file component
        s3.upload_file("/home/jovyan/work/awspython/text.txt",
                      args_dict.get('destination_bucket_name'),
                      args_dict.get('destination_file_name'))
        print("upload successful")
    except Exception as e:
        print(e)

#starting point for the execution of program
if __name__== "__main__":

    sc = SparkContext("local", "python_aws_s3")
    sqlContext = SQLContext(sc)

    parser = argparse.ArgumentParser(description="aws_python_args")

    parser.add_argument('access_key', help="access key")
    parser.add_argument('secret_key', help="secret key")
    parser.add_argument('source_bucket_name', help="bucket name")
    parser.add_argument('source_file_name', help="file name")
    parser.add_argument('destination_bucket_name', help='destination bucket name')
    parser.add_argument('destination_file_name', help='destination file name')
    parser.add_argument('filter_key1', help="filter key 1")
    parser.add_argument('filter_key2', help="filter key 2")

    args = parser.parse_args()
    print(vars(args))
    args_dict = vars(args)

    s3=s3_login()
    record_data=s3_data_fetch(s3)
    data_processing(record_data)
