## Docker-PySpark with awscli (Jupiter Notebook) 
### For providing runtime arguments we are using argparse


 #### 1. Build an image from Dockerfile
Run the following command to build an image. Here aws/python is image repo name
```
sudo docker build -t aws/python .
 ```

 #### 2. Start the container
 Run the following command to start the container and to get into bash prompt
 get the image id from the command ```sudo docker images```
 ```
sudo docker run -it  --name [type container_name] [image-id]
```
#### 3. Pass all the  parameters in the executed shell that are required by python script (It uses Argparse)
#### Argparse: 
   Parser for command line options, arguments and subcommands.
##
### S3 select:
#### Reference links:

https://medium.com/@bobhaffner/aws-s3-select-retrieving-subsets-of-tabular-files-7df36cd2d230

https://www.linkedin.com/pulse/how-i-used-amazon-s3-select-query-selective-data-stored-sharma/

S3 Select is an Amazon **S3** capability designed to pull out only the data you need from an object, which can dramatically improve the performance and reduce the cost of applications that need to access data in **S3** Amazon. **S3 Select** works on objects stored in CSV and JSON format.

#### Scenario where can use s3 select:

 If we need to analyze the CSV/JSON data sitting in S3, but the data for all ~200 applications is saved in a new GZIP-ed CSV/JSON every day. Without S3 Select, we need to download, decompress and process the entire CSV to get the data we needed. With S3 Select, we can use a simple SQL expression to return only the data from the application we’re interested in, instead of retrieving the entire object. This means we’re dealing with an order of magnitude less data which improves the performance of our underlying applications.
 
 #### Code:
 To establish connection with AWS s3
 ````
 import boto3
 try:  
    s3 = boto3.resource(  
        "s3",  
        aws_access_key_id="",  
        aws_secret_access_key="")  
except Exception as e:  
    print(e)
````
 A) S3 Select allows us to use SQL to fetch only the data we need.

 1) Bucket is the bucket name in s3, 
 
 2) Key is the object name in s3 bucket, 
 
 3) Expression is the SQL Expression, 
 
 4) CompressionType is 'None' if file not compressed or else 'GZIP'
 
 5) FileHeaderInfo is 'None' if we do not use any columns or else      'USE' 
 
 ex: select * from s3Object s , Here we do not use specific column
 So FileHeaderInfo is None
 
  select * from s3Object s where s.city='Bangalore' , Here use specific column so FileHeaderInfo is 'USE'
  
  6) FieldDelimiter is the delimiter in our object ex: comma,colon
   
 ````
 r = s3.select_object_content(  
    Bucket="sample.datasets",  
    Key="temperature.csv",  
    ExpressionType='SQL',  
    Expression=  
    "select * from s3Object s limit 10",  
    InputSerialization={  
        'CompressionType': 'None',
        'CSV': {  
            "FileHeaderInfo": "None",  
            "FieldDelimiter": ","  
  }  
    },  
    OutputSerialization={  
        'CSV': {  
            "FieldDelimiter": ","  
  }}, )
````
B) The select_object_content method returns an AWS EventStream that we have to loop through to get your data.
We convert the byte strings into regular strings and then join all the records to form one large string.
````
record_data = []  
for event in r['Payload']:  
    if 'Records' in event:  
        record_data.append(event['Records']['Payload'])  
  
decoded_records = ''.join(r.decode("utf-8") for r in record_data)
````
C)  We then use StringIO() so Pandas read_csv() can read it just like a file off disk.

````
import pandas as pd
from io import StringIO

pandas_df = pd.read_csv(StringIO(record_data), sep='[,]', header=0)
````
