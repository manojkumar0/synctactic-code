## Docker-PySpark with awscli (Jupiter Notebook)


###  How to install on ubuntu
---
 #### 1. Get the latest update of packages
  ```
  sudo apt-get update
 ```
 #### 2. Make a new Directory and create a  Dockerfile
  Copy the dockerfile commands from path  ```s3_pyspark/Dockerfile```
 
 #### 3. Build an image from Dockerfile
Run the following command to build an image. Here aws/python is image repo name
```
sudo docker build -t aws/python .
 ```
 #### 4. Start the container
 Run the following command to start the container and get a bash prompt
 get the image id from command ```sudo docker images```
 ```
sudo docker run -it [image-id] bash
```

 #### 5. Verify installation 
 ```aws --version```

#### 6. Get aws credentials from aws console
We can add these credentials in many ways.
 1) Add these credentials to credentials file which is hidden by using command:
 ```aws configure```
 2) By hardcoding in the python code (not recommended)
 3) By passing credentials at runtime using argparser 
 #### 5. Execute pyspark code
 #### boto3 : 
 is the Amazon Web Services (AWS) Software Development Kit (SDK) for Python, 
 which allows Python developers to write software that makes use of services 
 like Amazon S3 and Amazon EC2.
  
 create a python file ``aws_python.py`` using boto3 to fetch data from s3 and 
 execute the command.
 ````/usr/local/spark/bin/spark-submit aws_python.py````

#### Code:
a) Here we are using session to get the credentials from credentials file which is hidden. There might be many credentials so we are using profile_name for specific user credentials    
````   
session = boto3.session.Session(profile_name="manu") 
    
access_key = session.get_credentials().access_key
secret_key = session.get_credentials().secret_key
  ````  

 b) Establishing connection with AWS s3 based on access_key and secret_key
  ````
try:  
    s3 = boto3.resource(  
        "s3",  
        aws_access_key_id=access_key,  
        aws_secret_access_key=secret_key)  
except Exception as e:  
    print(e)

````

c) Fetching s3 object based on bucket_name and key. Here key
   is the name of the file in s3 bucket. After that we decode the 
   obtained object(converting byte string to regular string).
   We then use StringIO() so Pandas read_csv() can read it just like a file off disk.
```` 
s3_binary_obj = s3.Object(bucket_name="sampleds", key="records.csv")  
      
s3_obj = StringIO(s3_binary_obj.get()['Body'].read().decode("utf-8"))  
````
 d) Creating pandas dataframe from the s3 decoded object with comma delimiter. Pandas dataframe considers first row as header because we mentioned header=0. 
````      
pandas_df = pd.read_csv(s3_obj, sep='[,]', header=0)  
 ````
 e) Converting pandas dataframe to spark dataframe using SQLContext and then applying filters
 ````     
 sc = SparkContext("local", "s3_bucket_data_processing_app")  
 sqlContext = SQLContext(sc)  
      
spark_df = sqlContext.createDataFrame(pandas_df)  

spark_df1 = spark_df.filter((spark_df['`Age in Yrs.`'] > 50) &  
                                    (spark_df['Year of Joining'] > 2012))  
````      
 f) Converting spark dataframe to pandas dataframe and saving the  dataframe as csv  without including index
````
pandas_df1 = spark_df1.toPandas()  
      
pandas_df1.to_csv(  
            "/home/jovyan/work/python_files/processed_records.csv", index=False)  
````
 g) For uploading file to s3 ,we are using client  because resource do not have upload_file component  
````    
  
try:   
  s3.meta.client.upload_file(  
        "/home/jovyan/work/python_files/processed_records.csv", "sampleds",  
        "processed_records.csv")  
  print("upload successful")  
except Exception as e:  
    print(e)

````
